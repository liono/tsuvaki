<?php
/**
 * Plugin Name: Jewish Calendar
 * Description: Jewish Calendar page
 * Version: 1.0.0
 * Author: Anatoly Khelmer
 */

function jcal_shortcode()
{
    wp_enqueue_style('jcal', '/wp-content/plugins/jcal/holidays-master/demo-style/demo-core.css');
    wp_enqueue_style('jcal2', '/wp-content/plugins/jcal/holidays-master/demo-style/demo.css');

    require_once 'holidays-master/calendar.php'; 
}


//add_action('wp_enqueue_scripts', 'get_calendar_javascript');
add_shortcode('jcal', 'jcal_shortcode');