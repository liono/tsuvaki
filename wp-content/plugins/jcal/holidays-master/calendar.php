<?php

date_default_timezone_set('Asia/Jerusalem'); // We have to set something or else PHP will complain.

require_once dirname(__FILE__) .'/lib/NativeCalendar.php'; // Provides the calendar object. The 'engine.'
require_once dirname(__FILE__) .'/demo.inc'; // Utility functions

// $now contains today's date, and will be highlighted on the calendar printed.
// You may wish to add to it the user's timezone offset.
$now = getdate(time());

$monthesList = array(
    '1' => 'Январь',
    '2' => 'Февраль',
    '3' => 'Март',
    '4' => 'Апрель',
    '5' => 'Май',
    '6' => 'Июнь',
    '7' => 'Июль',
    '8' => 'Август',
    '9' => 'Сентябрь',
    '10' => 'Октябрь',
    '11' => 'Ноябрь',
    '12' => 'Декабрь'
);

// var_dump();
// die();
//
// Step 1:
//
// Load parameters from the URL.
//

// The year to show the callendar for. If it isn't provided in the URL, use current year.
$year  = get_param('y', $now['year']);
$_GET['y'] = $year;
// The month to show the calendar for. If it isn't provided in the URL, use current month.
$month = get_param('month', $now['mon']);
$_GET['month'] = $month;


// The language in which to show the calendar. Defaults to Hebrew if and only if the browser
// tells us the user reads Hebrew.
$language = get_param('language', strstr(@$_SERVER['HTTP_ACCEPT_LANGUAGE'], 'he') ? 'he' : 'en');

// The method used to calculate the holidays. Can be either 'israel' or 'diaspora'. Defaults
// to 'israel' if the language used is Hebrew.
$method = get_param('method', $language == 'he' ? 'israel' : 'diaspora');

// Show 'Erev Rosh HaShana', etc. Defaults to true.
$eves = get_param('eves', '1');

// Show Sefirat HaOmer (from Passover to Shavuot). Defaults to false.
$sefirat_omer = get_param('sefirat_omer', '0');

// Show 'Isru Khags'. Defaults to false because they have almost no halakhic meaning.
$isru = get_param('isru', '0');

//
// Step 2:
//
// Instantiate the calendar object.
//

function t($s) {
    static $table = array(
        'Tishrei' => 'Тишрей',
        'Heshvan' => 'Хешван',
        'Kislev' => 'Кислев',
        'Tevet' => 'Тевет',
        'Shevat' => 'Шват',
        'Adar' => 'Адар',
        'Adar II' => 'Адар II',
        'Nisan' => 'Нисан',
        'Iyar' => 'Ияр',
        'Sivan' => 'Сиван',
        'Tamuz' => 'Тамуз',
        'Av' => 'Ав',
        'Elul' => 'Элуль',
        'Adar I' => 'Адар I',
        'Erev Rosh HaShana' => 'Канун Рош аШана',
        'Rosh HaShana I' => 'Рош аШана I',
        'Rosh HaShana II' => 'Рош аШана II',
        'Tsom Gedalya' => 'Цом Гдалия',
        'Erev Yom Kippur' => 'Канун Йом Кипур',
        'Yom Kippur' => 'Йом Кипур',
        'Erev Sukkot' => 'Канун Суккот',
        'Sukkot' => 'Суккот',
        'Sukkot II (Diaspora)' => 'Суккот II (Диаспора)',
        'Khol HaMoed Sukkot' => 'Холь аМоед Суккот',
        'Hoshana Rabba' => 'Ошана Раба',
        'Shemini Atseret' => 'Шмини Ацерет',
        'Simkhat Tora' => 'Симхат Тора',
        'Khanukka I' => 'Ханука I',
        'Khanukka II' => 'Ханука II',
        'Khanukka III' => 'Ханука III',
        'Khanukka IV' => 'Ханука IV',
        'Khanukka V' => 'Ханука V',
        'Khanukka VI' => 'Ханука VI',
        'Khanukka VII' => 'Ханука VII',
        'Khanukka VIII' => 'Ханука VIII',
        'Tsom Tevet' => 'Цом Тевет',
        'Taanit Esther' => 'Таанит Эстер',
        'Purim' => 'Пурим',
        'Shushan Purim' => 'Шушан Пурим',
        'Erev Pesakh' => 'Канун Песах',
        'Pesakh' => 'Песах',
        'Pesakh II (Diaspora)' => 'Песах II (Диаспора)',
        'Khol HaMoed Pesakh' => 'Холь аМоед Песах',
        'Pesakh VII' => 'Песах VII',
        'Pesakh VIII (Diaspora)' => 'Песах VIII (Диаспора)',
        'Sefirat HaOmer' => 'Сфират аОмер',
        'Yom HaShoa' => 'Йом аШоа',
        'Yom HaZikaron' => 'Йом аЗикарон',
        'Yom HaAtsmaut' => 'Йом аАцмаут',
        'Lag BaOmer' => 'Лаг баОмер',
        'Yom Yerushalayim' => 'Йом Иерушалаим',
        'Erev Shavuot' => 'Канун Шавуот',
        'Shavuot' => 'Шавуот',
        'Tsom Tamuz' => 'Цом Тамуз',
        'Tisha BeAv' => 'Теша беАв',
        'Tu BiShevat' => 'Ту биШват',
        'Sun' => 'Вскр',
        'Mon' => 'Пон',
        'Tue' => 'Втр',
        'Wed' => 'Ср',
        'Thu' => 'Чтв',
        'Fri' => 'Пт',
        'Sat' => 'Сбт'
    );
    
    if (isset($table[$s])) {
        return $table[$s];
    }
    else {
        return $s;
    }
}

$jcal = NativeCalendar::factory('Jewish');
$jcal->settings(array(
  //'language' => ($language == 'he' ? CAL_LANG_NATIVE : CAL_LANG_FOREIGN),
  'language' => 'ru',
  'method' => $method,
  'sefirat_omer' => $sefirat_omer,
  'eves' => $eves,
  'isru' => $isru,
));

//
// Step 3:
//
// Print the page.
//

$javascript = get_demo_javascript();
$direction = ($language == 'he' ? 'rtl' : 'ltr');

$options = array();
foreach (range($year - 70, $year + 70) as $y) {
    $options[$y] = $y;
}


print($javascript);

$start_date_str = $jcal->getMediumDate(array('year'=>$year, 'mon'=>$month, 'mday'=>1));
$end_date_str   = $jcal->getMediumDate(array('year'=>$year, 'mon'=>$month, 'mday'=>@cal_days_in_month(CAL_GREGORIAN, $month, $year)));

?>

<div class="col_12">
    <div>
    <?php print_link(trans('Previous year', 'השנה הקודמת'), create_url($year - 1, $month), back_arrow());?>
    <?php print_select_element('y', $options);?>
    <?php print_link(trans('Next year', 'השנה הבאה'), create_url($year + 1, $month), forward_arrow());?>
    </div>
    
    <div>
    <?php print_link(trans('Previous month', 'החודש הקודם'), create_url($year, $month - 1), back_arrow());?>
    <?php print_select_element('month', $monthesList);?>
    <?php print_link(trans('Next month', 'החודש הבא'), create_url($year, $month + 1), forward_arrow());?>
    </div>
</div>

<div class="col_12_last">
    <h1>
        <?php echo $start_date_str . " &#x2013; " . $end_date_str; ?>
    </h1>
</div>

<?php
print $jcal->printCal($year, $month);
