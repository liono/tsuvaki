function E(id) 
{ 
  if (document.getElementById) 
    return document.getElementById(id); 
  else 
    return document.all[id]; 
} 

function get_selected(id)
{
  var sel_obj = E(id);
  if (!sel_obj) {
    return '';
  }
  var val = '';
  for (var i = 0; i < sel_obj.options.length; i++) {
    if (sel_obj.options[i].selected)
      val = sel_obj.options[i].value;
  }
  return val;
}

function change_date()
{
  var params = [ 'year', 'month', 'language', 'method', 'eves', 'sefirat_omer', 'isru' ];
  var href = "{$_SERVER['PHP_SELF']}?";
  for (var i = 0; i < params.length; i++) {
    href += params[i] + '=' + get_selected(params[i]) + '&';
  }
  window.location = href;
}